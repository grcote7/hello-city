@extends('layouts/app')

@section('title')
    {{config('app.name')}}
@endsection

@section('content')
    <img src="{{asset('/images/tof.jpg')}}" alt="image 1" class="rounded shadow-md h-32 mt-12">

    <h1 class="mt-5 sm:text-5xl text-3xl font-semibold text-indigo-600">Hello from Casablanca</h1>

    <p class=text-lg text-gray-800>It's currently {{date('h:i A')}} </p>

@endsection
