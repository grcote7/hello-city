@extends('layouts/app')
  


@section('title')
About-us | {{config('app.name')}} 
@endsection


@section('content')
   <img src="{{asset('/images/téléchargement.jpg')}}" alt="image 2" class="rounded-full shadow-md h-32 my-12">

   <h2 class="text-gray-700">
      Built with <span class="text-pink-500">&hearts;</span> by LES TEACHERS DU NET.
   </h2>

   <p class="mt-5">
      <a href="{{route('home')}}" class="text-indigo-500 hover:text-indigo-600 underline">
         Retour à la page d'accueil
      </a>
   </p>

@endsection